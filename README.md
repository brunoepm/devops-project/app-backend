## Express (Node.js) Application

Sample app used for pipeline testing taken from https://github.com/LukeMwila/react-express-postgres
No changes to the original application, just added some manifests to deploy the app on kubernetes and a basic pipeline to build the docker image and deploy the app on the cluster
